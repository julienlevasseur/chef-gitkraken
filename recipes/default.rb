#
# Cookbook:: gitkraken
# Recipe:: default
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

include_recipe 'gitkraken::debian' if platform_family?('debian')
