#
# Cookbook:: chef-gitkraken
# Recipe:: debian
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

%w(libgnome-keyring-common libgnome-keyring-dev).each do |pkg|
  package pkg
end

remote_file '/tmp/gitkraken-amd64.deb' do
  source 'https://release.gitkraken.com/linux/gitkraken-amd64.deb'
  mode 0644
  not_if 'dpkg -l|grep gitkraken'
end

dpkg_package "gitkraken" do
  source '/tmp/gitkraken-amd64.deb'
  action :install
  only_if { ::File.exist?('/tmp/gitkraken-amd64.deb') }
end
